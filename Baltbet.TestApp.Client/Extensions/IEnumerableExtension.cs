﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Baltbet.TestApp.Client.Extensions
{
    public static class IEnumerableExtension
    {
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> collection)
        {
            return new ObservableCollection<T>(collection);
        }
    }
}
