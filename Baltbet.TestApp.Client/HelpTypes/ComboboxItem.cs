﻿namespace Baltbet.TestApp.Client.HelpTypes
{
    public class ComboboxItem <T>
    {
        public string Text { get; set; }

        public T Value { get; set; }
    }
}
