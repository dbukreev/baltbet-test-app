﻿using System;
using System.Linq;
using System.Windows;
using Baltbet.TestApp.Client.DbService;
using Baltbet.TestApp.Client.Properties;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace Baltbet.TestApp.Client.Model
{
    public class AddMoneyModel: ModelBase
    {
        private decimal _amount;
        private bool? _dialogResult;

        public AddMoneyModel()
        {
            InitModel();
        }

        public decimal Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                if (_amount != value)
                {
                    _amount = value < 0 ? 0 : value;
                    RaisePropertyChanged("Amount");
                }
            }
        }

        public bool? DialogResult
        {
            get
            {
                return _dialogResult;
            }
            set
            {
                if (_dialogResult != value)
                {
                    _dialogResult = value;
                    RaisePropertyChanged("DialogResult");
                }
            }
        }


        private void InitModel()
        {
            Amount = 1000;
        }

        public async void AddMoneyToAccount()
        {
            var dbService = new DbServiceClient();

            try
            {
                await dbService.AddMoneyToAccountAsync(Settings.Default.AccountId, Amount);
            }
            catch (Exception ex)
            {
                var metroWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive) as MetroWindow;
                await metroWindow.ShowMessageAsync("Ошибка", $"Ошибка соединения с Сервисом БД. {ex}");
                Application.Current.Shutdown();
            }
            finally
            {
                dbService.Close();
            }

            CloseWindow();
        }

        public void CloseWindow()
        {
            DialogResult = true;
        }
    }
}
