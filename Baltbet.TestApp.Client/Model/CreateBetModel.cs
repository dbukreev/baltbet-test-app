﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Baltbet.TestApp.Client.DbService;
using Baltbet.TestApp.Client.Properties;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace Baltbet.TestApp.Client.Model
{
    public class CreateBetModel: ModelBase
    {
        private BetType _selectedBetType;
        private Visibility _visibilityFormula;
        private Visibility _visibilityEditionNumber;
        private bool? _dialogResult;

        public CreateBetModel()
        {
            InitModel();
        }

        public decimal Amount { get; set; }

        public decimal CurrentAccountAmount { get; set; }

        public string Formula { get; set; }

        public int EditNumber { get; set; }

        public List<BetType> BetTypes { get; set; }

        public BetType SelectedBetType
        {
            get
            {
                return _selectedBetType;
            }
            set
            {
                if (_selectedBetType != value)
                {
                    _selectedBetType = value;
                    ChangeControlsVisibility();
                }
            }
        }

        public Visibility VisibilityFormula
        {
            get
            {
                return _visibilityFormula;
            }
            set
            {
                if (_visibilityFormula != value)
                {
                    _visibilityFormula = value;
                    RaisePropertyChanged("VisibilityFormula");
                }
            }
        }

        public Visibility VisibilityEditionNumber
        {
            get
            {
                return _visibilityEditionNumber;
            }
            set
            {
                if (_visibilityEditionNumber != value)
                {
                    _visibilityEditionNumber = value;
                    RaisePropertyChanged("VisibilityEditionNumber");
                }
            }
        }

        public bool? DialogResult
        {
            get
            {
                return _dialogResult;
            }
            set
            {
                if (_dialogResult != value)
                {
                    _dialogResult = value;
                    RaisePropertyChanged("DialogResult");
                }
            }
        }


        private void InitModel()
        {
            var dbService = new DbServiceClient();

            try
            {
                BetTypes = dbService.GetBetTypes().ToList();
                var account = dbService.GetAccountById(Settings.Default.AccountId);

                SelectedBetType = BetTypes.First(_ => _.BetTypeId == 1);
                CurrentAccountAmount = account.Amount;
                Amount = CurrentAccountAmount < 1000 ? CurrentAccountAmount : 1000;
            }
            catch (Exception ex)
            {
                var metroWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive) as MetroWindow;
                metroWindow.ShowMessageAsync("Ошибка", $"Ошибка соединения с Сервисом БД. {ex}");
                Application.Current.Shutdown();
            }
            finally
            {
                dbService.Close();
            }
        }

        private void ChangeControlsVisibility()
        {
            switch (SelectedBetType.BetTypeId)
            {
                //обычная
                case 1:
                    {
                        VisibilityFormula = Visibility.Collapsed;
                        VisibilityEditionNumber = Visibility.Collapsed;
                        break;
                    }
                //система
                case 2:
                    {
                        VisibilityFormula = Visibility.Visible;
                        VisibilityEditionNumber = Visibility.Collapsed;
                        break;
                    }
                //суперэкспресс
                case 3:
                    {
                        VisibilityFormula = Visibility.Collapsed;
                        VisibilityEditionNumber = Visibility.Visible;
                        break;
                    }
                default:
                    {
                        VisibilityFormula = Visibility.Collapsed;
                        VisibilityEditionNumber = Visibility.Collapsed;
                        break;
                    }
            }
        }

        public async void CreateBet()
        {
            if (Amount <= decimal.Zero)
            {
                var metroWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive) as MetroWindow;
                var messageDialogSetting = new MetroDialogSettings
                {
                    DialogTitleFontSize = 16
                };
                await metroWindow.ShowMessageAsync("Внимание", "Ставка должна быть > 0", MessageDialogStyle.Affirmative, messageDialogSetting);
                return;
            }

            var bet = new Bet
            {
                Date = DateTime.Now,
                Amount = this.Amount,
                ResultId = 3,
                WinAmount = 0,
                TypeId = SelectedBetType.BetTypeId,
                Formula = SelectedBetType.BetTypeId == 2 ? this.Formula : null,
                EditionNumber = SelectedBetType.BetTypeId == 3 ? this.EditNumber : (int?) null,
            };

            var dbService = new DbServiceClient();

            try
            {
                await dbService.CreateBetAsync(bet);
                await dbService.AddMoneyToAccountAsync(Settings.Default.AccountId, (-1) * Amount);
            }
            catch (Exception ex)
            {
                var metroWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive) as MetroWindow;
                await metroWindow.ShowMessageAsync("Ошибка", $"Ошибка соединения с Сервисом БД. {ex}");
                Application.Current.Shutdown();
            }
            finally
            {
                dbService.Close();
            }

            //закрыть окно только после выполнения асинхронных вызовов.
            CloseWindow();
        }

        public void CloseWindow()
        {
            DialogResult = true;
        }
    }
}