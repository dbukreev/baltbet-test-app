﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Baltbet.TestApp.Client.DbService;
using Baltbet.TestApp.Client.HelpTypes;
using Baltbet.TestApp.Client.Extensions;
using Baltbet.TestApp.Client.Properties;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace Baltbet.TestApp.Client.Model
{
    public class MainModel: ModelBase
    {
        private Account _account;
        private ObservableCollection<Bet> _bets;
        private DateTime _startDate;
        private DateTime _endDate;
        private ComboboxItem<byte?> _selectedBetType;
        private ComboboxItem<byte?> _selectedBetResult;
        private bool _isInit;

        public MainModel()
        {
            InitModel();
        }

        public Account Account
        {
            get
            {
                return _account;
            }
            set
            {
                _account = value;
                RaisePropertyChanged("Account");
            }
        }

        public ObservableCollection<Bet> Bets
        {
            get
            {
                return _bets;
            }
            set
            {
                _bets = value;
                RaisePropertyChanged("Bets");
            }
        }

        public DateTime StartDate
        {
            get
            {
                return _startDate;
            } 
            set
            {
                if (_startDate != value)
                {
                    _startDate = value;

                    if (!_isInit)
                    {
                        UpdateBets();
                    }
                }
            }
        }

        public DateTime EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                if (_endDate != value)
                {
                    //берем весь последний день с учетом времени
                    _endDate = value.Date.AddDays(1).AddTicks(-1);

                    if (!_isInit)
                    {
                        UpdateBets();
                    }
                }
            }
        }

        public List<ComboboxItem<byte?>> BetTypes { get; set; }

        public List<ComboboxItem<byte?>> BetResults { get; set; }

        public ComboboxItem<byte?> SelectedBetType
        {
            get
            {
                return _selectedBetType;
            }
            set
            {
                if (_selectedBetType != value)
                {
                    _selectedBetType = value;

                    if (!_isInit)
                    {
                        UpdateBets();
                    }
                }
            }
        }

        public ComboboxItem<byte?> SelectedBetResult
        {
            get
            {
                return _selectedBetResult;
            }
            set
            {
                if (_selectedBetResult != value)
                {
                    _selectedBetResult = value;

                    if (!_isInit)
                    {
                        UpdateBets();
                    }
                }
            }
        }


        private void InitModel()
        {
            _isInit = true;

            StartDate = new DateTime(2017, 10, 1);
            EndDate = DateTime.Today;

            var betTypes = new List<BetType>();
            var betResults = new List<BetResult>();

            var dbService = new DbServiceClient();

            try
            {
                betTypes = dbService.GetBetTypes().ToList();
                betResults = dbService.GetBetResults().ToList();

                Account = dbService.GetAccountById(Settings.Default.AccountId);
                Bets = dbService.GetBetsByFilter(StartDate, EndDate, null, null).ToObservableCollection();
            }
            catch (Exception ex)
            {
                var metroWindow = (Application.Current.MainWindow as MetroWindow);
                metroWindow.ShowMessageAsync("Ошибка", $"Ошибка соединения с Сервисом БД. {ex}");
                Application.Current.Shutdown();
            }
            finally
            {
                dbService.Close();
            }

            var allItem = new ComboboxItem<byte?>
            {
                Text = "Все",
                Value = null
            };

            BetTypes = betTypes
                .Select(_ => new ComboboxItem<byte?>
                {
                    Value = _.BetTypeId,
                    Text = _.Name
                })
                .ToList();
            BetTypes.Insert(0, allItem);

            BetResults = betResults
                .Select(_ => new ComboboxItem<byte?>
                {
                    Value = _.BetResultId,
                    Text = _.Name
                })
                .ToList();
            BetResults.Insert(0, allItem);

            SelectedBetType = BetTypes.First(_ => !_.Value.HasValue);
            SelectedBetResult = BetResults.First(_ => !_.Value.HasValue);

            _isInit = false;
        }

        public async void UpdateBets()
        {
            var dbService = new DbServiceClient();

            try
            {
                Bets = (await dbService.GetBetsByFilterAsync(StartDate, EndDate, SelectedBetResult.Value, SelectedBetType.Value)).ToObservableCollection();
            }
            catch (Exception ex)
            {
                var metroWindow = (Application.Current.MainWindow as MetroWindow);
                await metroWindow.ShowMessageAsync("Ошибка", $"Ошибка соединения с Сервисом БД. {ex}");
                Application.Current.Shutdown();
            }
            finally
            {
                dbService.Close();
            }
        }

        public async void UpdateAccount()
        {
            var dbService = new DbServiceClient();

            try
            {
                Account = await dbService.GetAccountByIdAsync(Settings.Default.AccountId);
            }
            catch (Exception ex)
            {
                var metroWindow = (Application.Current.MainWindow as MetroWindow);
                await metroWindow.ShowMessageAsync("Ошибка", $"Ошибка соединения с Сервисом БД. {ex}");
                Application.Current.Shutdown();
            }
            finally
            {
                dbService.Close();
            }
        }
    }
}
