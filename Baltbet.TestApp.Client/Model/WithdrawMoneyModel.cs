﻿using System;
using System.Linq;
using System.Windows;
using Baltbet.TestApp.Client.DbService;
using Baltbet.TestApp.Client.Properties;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace Baltbet.TestApp.Client.Model
{
    public class WithdrawMoneyModel : ModelBase
    {
        private decimal _amount;
        private bool? _dialogResult;

        public WithdrawMoneyModel()
        {
            InitModel();
        }

        public decimal Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                if (_amount != value)
                {
                    _amount = value < 0 ? 0 : value;
                    RaisePropertyChanged("Amount");
                }
            }
        }

        public bool? DialogResult
        {
            get
            {
                return _dialogResult;
            }
            set
            {
                if (_dialogResult != value)
                {
                    _dialogResult = value;
                    RaisePropertyChanged("DialogResult");
                }
            }
        }

        public decimal MaxValue { get; set; }


        private void InitModel()
        {
            var dbService = new DbServiceClient();

            try
            {
                var account = dbService.GetAccountById(Settings.Default.AccountId);

                Amount = account.Amount;
                MaxValue = account.Amount;
            }
            catch (Exception ex)
            {
                var metroWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive) as MetroWindow;
                metroWindow.ShowMessageAsync("Ошибка", $"Ошибка соединения с Сервисом БД. {ex}");
                Application.Current.Shutdown();
            }
            finally
            {
                dbService.Close();
            }
        }

        public async void WithdrawMoneyModelFromAccount()
        {
            var dbService = new DbServiceClient();

            try
            {
                await dbService.AddMoneyToAccountAsync(Settings.Default.AccountId, (-1) * Amount);
            }
            catch (Exception ex)
            {
                var metroWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive) as MetroWindow;
                await metroWindow.ShowMessageAsync("Ошибка", $"Ошибка соединения с Сервисом БД. {ex}");
                Application.Current.Shutdown();
            }
            finally
            {
                dbService.Close();
            }

            CloseWindow();
        }

        public void CloseWindow()
        {
            DialogResult = true;
        }
    }
}
