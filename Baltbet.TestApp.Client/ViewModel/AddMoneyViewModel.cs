﻿using System.Windows.Input;
using Baltbet.TestApp.Client.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace Baltbet.TestApp.Client.ViewModel
{
    public class AddMoneyViewModel: ViewModelBase
    {
        public AddMoneyViewModel()
        {
            AddMoneyModel = new AddMoneyModel();

            AddMoneyCommand = new RelayCommand(OnAddMoney);
            CancelCommand = new RelayCommand(OnCloseWindow);
            CleanUpCommand = new RelayCommand(CleanUpViewModel);
        }

        public AddMoneyModel AddMoneyModel { get; set; }

        public ICommand AddMoneyCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public ICommand CleanUpCommand { get; set; }


        private void OnAddMoney()
        {
            AddMoneyModel.AddMoneyToAccount();
        }

        private void OnCloseWindow()
        {
            AddMoneyModel.CloseWindow();
        }

        private void CleanUpViewModel()
        {
            ViewModelLocator.CleanUpAddMoney();
        }
    }
}
