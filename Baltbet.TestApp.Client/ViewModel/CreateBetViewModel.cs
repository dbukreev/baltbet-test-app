﻿using System.Windows.Input;
using Baltbet.TestApp.Client.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Baltbet.TestApp.Client.ViewModel
{
    public class CreateBetViewModel: ViewModelBase
    {
        public CreateBetViewModel()
        {
            CreateBetModel = new CreateBetModel();

            CreateBetCommand = new RelayCommand(OnCreateBet);
            CancelCommand = new RelayCommand(OnCloseWindow);
            CleanUpCommand = new RelayCommand(CleanUpViewModel);
        }

        public CreateBetModel CreateBetModel { get; set; }

        public ICommand CreateBetCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public ICommand CleanUpCommand { get; set; }


        private void OnCreateBet()
        {
            CreateBetModel.CreateBet();
        }

        private void OnCloseWindow()
        {
            CreateBetModel.CloseWindow();
        }

        private void CleanUpViewModel()
        {
            ViewModelLocator.CleanUpCreateBet();
        }
    }
}
