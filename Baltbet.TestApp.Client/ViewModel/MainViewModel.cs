using Baltbet.TestApp.Client.Model;
using GalaSoft.MvvmLight;
using System.Windows.Input;
using Baltbet.TestApp.Client.View;
using GalaSoft.MvvmLight.CommandWpf;

namespace Baltbet.TestApp.Client.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        public MainViewModel()
        {
            MainModel = new MainModel();

            AddMoneyCommand = new RelayCommand(OnAddMoneyCommand);
            WithdrawMoneyCommand = new RelayCommand(OnWithdrawMoneyCommand);
            CreateBetCommand = new RelayCommand(OnCreateBetCommand);
        }

        public MainModel MainModel { get; set; }

        public ICommand AddMoneyCommand { get; set; }

        public ICommand WithdrawMoneyCommand { get; set; }

        public ICommand CreateBetCommand { get; set; }


        private void OnAddMoneyCommand()
        {
            var addMoneyWindow = new AddMoneyWindow();
            addMoneyWindow.ShowDialog();

            MainModel.UpdateAccount();
        }

        private void OnWithdrawMoneyCommand()
        {
            var withdrawMoneyWindow = new WithdrawMoneyWindow();
            withdrawMoneyWindow.ShowDialog();

            MainModel.UpdateAccount();
        }

        private void OnCreateBetCommand()
        {
            var createBetWindow = new CreateBetWindow();
            createBetWindow.ShowDialog();

            MainModel.UpdateBets();
            MainModel.UpdateAccount();
        }
    }
}