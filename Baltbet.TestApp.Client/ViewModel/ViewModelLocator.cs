using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace Baltbet.TestApp.Client.ViewModel
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<AddMoneyViewModel>();
            SimpleIoc.Default.Register<WithdrawMoneyViewModel>();
            SimpleIoc.Default.Register<CreateBetViewModel>();
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public AddMoneyViewModel AddMoney
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AddMoneyViewModel>();
            }
        }

        public WithdrawMoneyViewModel WithdrawMoney
        {
            get
            {
                return ServiceLocator.Current.GetInstance<WithdrawMoneyViewModel>();
            }
        }

        public CreateBetViewModel CreateBet
        {
            get
            {
                return ServiceLocator.Current.GetInstance<CreateBetViewModel>();
            }
        }


        public static void CleanUpAddMoney()
        {
            SimpleIoc.Default.Unregister<AddMoneyViewModel>();
            SimpleIoc.Default.Register<AddMoneyViewModel>();
        }

        public static void CleanUpWithdrawMoney()
        {
            SimpleIoc.Default.Unregister<WithdrawMoneyViewModel>();
            SimpleIoc.Default.Register<WithdrawMoneyViewModel>();
        }

        public static void CleanUpCreateBet()
        {
            SimpleIoc.Default.Unregister<CreateBetViewModel>();
            SimpleIoc.Default.Register<CreateBetViewModel>();
        }
    }
}