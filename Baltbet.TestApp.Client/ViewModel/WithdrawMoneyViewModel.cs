﻿using System.Windows.Input;
using Baltbet.TestApp.Client.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace Baltbet.TestApp.Client.ViewModel
{
    public class WithdrawMoneyViewModel : ViewModelBase
    {
        public WithdrawMoneyViewModel()
        {
            WithdrawMoneyModel = new WithdrawMoneyModel();

            WithdrawMoneyCommand = new RelayCommand(OnAddMoney);
            CancelCommand = new RelayCommand(OnCloseWindow);
            CleanUpCommand = new RelayCommand(CleanUpViewModel);
        }

        public WithdrawMoneyModel WithdrawMoneyModel { get; set; }

        public ICommand WithdrawMoneyCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public ICommand CleanUpCommand { get; set; }


        private void OnAddMoney()
        {
            WithdrawMoneyModel.WithdrawMoneyModelFromAccount();
        }

        private void OnCloseWindow()
        {
            WithdrawMoneyModel.CloseWindow();
        }

        private void CleanUpViewModel()
        {
            ViewModelLocator.CleanUpWithdrawMoney();
        }
    }
}
