﻿CREATE TABLE [dbo].[Account]
(
    [Id]        INT IDENTITY(1, 1) NOT NULL,
    [Name]      NVARCHAR(255)      NOT NULL,
    [Amount]    MONEY              NOT NULL,
    CONSTRAINT [PK_Account_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
)
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Учетные записи',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Account'
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Идентификатор аккаунта',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Account',
    @level2type = N'COLUMN',
    @level2name = N'Id'
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'ФИО пользователя',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Account',
    @level2type = N'COLUMN',
    @level2name = N'Name'
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Сумма на счету',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Account',
    @level2type = N'COLUMN',
    @level2name = N'Amount'
GO