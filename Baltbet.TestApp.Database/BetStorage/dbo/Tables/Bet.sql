﻿CREATE TABLE [dbo].[Bet]
(
    [Id]            INT IDENTITY(1, 1) NOT NULL,
    [Date]          DATETIME           NOT NULL,
    [Amount]        MONEY              NOT NULL,
    [Result]        TINYINT            NOT NULL,
    [WinAmount]     MONEY              NOT NULL,
    [Type]          TINYINT            NOT NULL,
    [Formula]       NVARCHAR(255)      NULL,
    [EditionNumber] INT                NULL,
    CONSTRAINT [PK_Bet_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
)
GO

CREATE NONCLUSTERED INDEX [IX_Bet_Date_Result_Type] ON [dbo].[Bet]
(
    [Date] ASC,
    [Result] ASC,
    [Type] ASC
)
INCLUDE ([Amount],
         [WinAmount],
         [Formula],
         [EditionNumber])
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Ставки',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Bet'
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Идентификатор ставки',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Bet',
    @level2type = N'COLUMN',
    @level2name = N'Id'
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Дата и время',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Bet',
    @level2type = N'COLUMN',
    @level2name = N'Date'
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Сумма ставки',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Bet',
    @level2type = N'COLUMN',
    @level2name = N'Amount'
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Результат ставки',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Bet',
    @level2type = N'COLUMN',
    @level2name = N'Result'
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Сумма выигрыша',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Bet',
    @level2type = N'COLUMN',
    @level2name = N'WinAmount'
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Тип ставки',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Bet',
    @level2type = N'COLUMN',
    @level2name = N'Type'
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Формула для типа ставки Система, иначе NULL',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Bet',
    @level2type = N'COLUMN',
    @level2name = N'Formula'
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Номер тиража для типа ставки Суперэкспресс, иначе NULL',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'Bet',
    @level2type = N'COLUMN',
    @level2name = N'EditionNumber'
GO