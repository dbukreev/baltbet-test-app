﻿CREATE TABLE [dbo].[BetResult]
(
    [Id]   TINYINT IDENTITY(1, 1) NOT NULL,
    [Name] NVARCHAR(255)          NOT NULL,
    CONSTRAINT [PK_BetResult_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
)
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Справочник типов результатов ставок',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'BetResult'
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Идентификатор типа ставки',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'BetResult',
    @level2type = N'COLUMN',
    @level2name = N'Id'
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
    @value      = N'Название',
    @level0type = N'SCHEMA',
    @level0name = N'dbo',
    @level1type = N'TABLE',
    @level1name = N'BetResult',
    @level2type = N'COLUMN',
    @level2name = N'Name'
GO