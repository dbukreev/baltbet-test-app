﻿using Baltbet.TestApp.DbService.Interfaces;
using Baltbet.TestApp.EFData;

namespace Baltbet.TestApp.DbService
{
    public partial class DbService : IDbService
    {
        private readonly BetStorageEntities _betStorageEntities;

        public DbService()
        {
            _betStorageEntities = new BetStorageEntities();
            _betStorageEntities.Configuration.AutoDetectChangesEnabled = false;
#if DEBUG
            _betStorageEntities.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
#endif
        }
    }
}
