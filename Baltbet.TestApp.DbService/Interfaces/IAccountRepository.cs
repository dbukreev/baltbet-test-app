﻿using System.ServiceModel;
using Baltbet.TestApp.Entities.BetStorage;

namespace Baltbet.TestApp.DbService.Interfaces
{
    [ServiceContract]
    public interface IAccountRepository
    {
        [OperationContract]
        void AddMoneyToAccount(int accountId, decimal amount);

        [OperationContract]
        Account GetAccountById(int accountId);
    }
}
