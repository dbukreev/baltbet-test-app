﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Baltbet.TestApp.Entities.BetStorage;

namespace Baltbet.TestApp.DbService.Interfaces
{
    [ServiceContract]
    public interface IBetRepository
    {
        [OperationContract]
        void CreateBet(Bet bet);

        [OperationContract]
        Bet GetBetById(int betId);

        [OperationContract]
        IEnumerable<Bet> GetBetsByFilter(DateTime dateFrom, DateTime dateTo, byte? result, byte? betType);
    }
}
