﻿using System.Collections.Generic;
using System.ServiceModel;
using Baltbet.TestApp.Entities.BetStorage;

namespace Baltbet.TestApp.DbService.Interfaces
{
    [ServiceContract]
    public interface IBetTypeRepository
    {
        [OperationContract]
        IEnumerable<BetType> GetBetTypes();
    }
}
