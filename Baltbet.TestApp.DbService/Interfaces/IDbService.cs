﻿using System.ServiceModel;

namespace Baltbet.TestApp.DbService.Interfaces
{
    [ServiceContract]
    public interface IDbService:
        IAccountRepository,
        IBetRepository,
        IBetTypeRepository,
        IBetResultRepository
    {
    }
}
