﻿using System;
using System.Linq;
using Baltbet.TestApp.Entities.BetStorage;

namespace Baltbet.TestApp.DbService
{
    public partial class DbService
    {
        /// <summary>
        /// Метод пополнения и снятия средств со счета аккаунта.
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта.</param>
        /// <param name="amount">Сумма (может быть отрицательной).</param>
        public void AddMoneyToAccount(int accountId, decimal amount)
        {
            var account = _betStorageEntities.Accounts.FirstOrDefault(_ => _.Id == accountId);

            if (account != null)
            {
                account.Amount += amount;

                _betStorageEntities.Entry(account).Property(_ => _.Amount).IsModified = true;
                _betStorageEntities.SaveChanges();
            }
            else
            {
                throw new Exception($"Учетная запись с Id = {accountId} не существует.");
            }
        }

        /// <summary>
        /// Метод получения аккаунта по Id.
        /// </summary>
        /// <param name="accountId">Идентификатор аккаунта.</param>
        public Account GetAccountById(int accountId)
        {
            var account = _betStorageEntities.Accounts.FirstOrDefault(_ => _.Id == accountId);

            if (account == null)
            {
                return null;
            }

            return new Account
            {
                AccountId = account.Id,
                Name = account.Name,
                Amount = account.Amount
            };
        }
    }
}