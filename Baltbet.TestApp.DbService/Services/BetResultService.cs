﻿using System.Collections.Generic;
using System.Linq;
using Baltbet.TestApp.Entities.BetStorage;

namespace Baltbet.TestApp.DbService
{
    public partial class DbService
    {
        /// <summary>
        /// Метод получения типов результатов ставок.
        /// </summary>
        public IEnumerable<BetResult> GetBetResults()
        {
            return _betStorageEntities.BetResults
                .Select(_ => new BetResult
                {
                    BetResultId = _.Id,
                    Name = _.Name
                })
                .OrderBy(_ => _.Name);
        }
    }
}