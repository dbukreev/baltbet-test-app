﻿using System;
using System.Collections.Generic;
using System.Linq;
using Baltbet.TestApp.Entities.BetStorage;

namespace Baltbet.TestApp.DbService
{
    public partial class DbService
    {
        /// <summary>
        /// Метод создания ставки.
        /// </summary>
        /// <param name="bet">Ставка.</param>
        public void CreateBet(Bet bet)
        {
            var newBet = new EFData.Bet
            {
                Date = bet.Date,
                Amount = bet.Amount,
                Result =  bet.ResultId,
                WinAmount = bet.WinAmount,
                Type = bet.TypeId,
                Formula = bet.Formula,
                EditionNumber = bet.EditionNumber
            };

            _betStorageEntities.Bets.Add(newBet);
            _betStorageEntities.SaveChanges();
        }

        /// <summary>
        /// Метод получения ставки по Id.
        /// </summary>
        /// <param name="betId">Идентификатор ставки.</param>
        public Bet GetBetById(int betId)
        {
            var bet = _betStorageEntities.Bets
                .Join(_betStorageEntities.BetResults, b => b.Result, br => br.Id, (b, br) => new {b, br})
                .Join(_betStorageEntities.BetTypes, bbr => bbr.b.Type, bt => bt.Id, (bbr, bt) => new {bbr, bt})
                .FirstOrDefault(_ => _.bbr.b.Id == betId);

            if (bet == null)
            {
                return null;
            }

            return new Bet
            {
                BetId = bet.bbr.b.Id,
                Date = bet.bbr.b.Date,
                Amount = bet.bbr.b.Amount,
                ResultId = bet.bbr.b.Result,
                ResultName = bet.bbr.br.Name,
                WinAmount = bet.bbr.b.WinAmount,
                TypeId = bet.bbr.b.Type,
                TypeName = bet.bt.Name,
                Formula = bet.bbr.b.Formula,
                EditionNumber = bet.bbr.b.EditionNumber
            };
        }

        /// <summary>
        /// Метод получения ставок по фильтру.
        /// </summary>
        /// <param name="dateFrom">Дата (время) от (включительно).</param>
        /// <param name="dateTo">Дата (время) до (включительно).</param>
        /// <param name="resultId">Идентификатор результата.</param>
        /// <param name="typeId">Идентификатор типа ставки.</param>
        public IEnumerable<Bet> GetBetsByFilter(DateTime dateFrom, DateTime dateTo, byte? resultId, byte? typeId)
        {
            return _betStorageEntities.Bets
                .Where(_ =>
                    (_.Date >= dateFrom) &&
                    (_.Date <= dateTo) &&
                    (!resultId.HasValue || _.Result == resultId.Value) &&
                    (!typeId.HasValue || _.Type == typeId.Value))
                .Join(_betStorageEntities.BetResults, b => b.Result, br => br.Id, (b, br) => new { b, br })
                .Join(_betStorageEntities.BetTypes, bbr => bbr.b.Type, bt => bt.Id, (bbr, bt) => new { bbr, bt })
                .OrderBy(_ => _.bbr.b.Date)
                .ThenBy(_ => _.bbr.br.Name)
                .ThenBy(_ => _.bt.Name)
                .Select(_ => new Bet
                {
                    BetId = _.bbr.b.Id,
                    Date = _.bbr.b.Date,
                    Amount = _.bbr.b.Amount,
                    ResultId = _.bbr.b.Result,
                    ResultName = _.bbr.br.Name,
                    WinAmount = _.bbr.b.WinAmount,
                    TypeId = _.bbr.b.Type,
                    TypeName = _.bt.Name,
                    Formula = _.bbr.b.Formula,
                    EditionNumber = _.bbr.b.EditionNumber
                });
        }
    }
}