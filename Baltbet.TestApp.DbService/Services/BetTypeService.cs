﻿using System.Collections.Generic;
using System.Linq;
using Baltbet.TestApp.Entities.BetStorage;

namespace Baltbet.TestApp.DbService
{
    public partial class DbService
    {
        /// <summary>
        /// Метод получения типов ставок.
        /// </summary>
        public IEnumerable<BetType> GetBetTypes()
        {
            return _betStorageEntities.BetTypes
                .Select(_ => new BetType
                {
                    BetTypeId = _.Id,
                    Name = _.Name
                })
                .OrderBy(_=>_.Name);
        }
    }
}