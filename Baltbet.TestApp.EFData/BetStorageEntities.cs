﻿using System.Configuration;
using System.Data.Entity.Core.EntityClient;

namespace Baltbet.TestApp.EFData
{
    public partial class BetStorageEntities
    {
        private static string _connectionString;

        public static string ConnectionString
        {
            get
            {
                if (_connectionString == null)
                {
                    string baseConnectionString = ConfigurationManager.ConnectionStrings["BetStorageEntities"].ConnectionString;

                    var entityBuilder = new EntityConnectionStringBuilder
                    {
                        Provider = "System.Data.SqlClient",
                        ProviderConnectionString = baseConnectionString,
                        Metadata =
                            @"res://*/BetStorageDb.csdl|res://*/BetStorageDb.ssdl|res://*/BetStorageDb.msl"
                    };

                    _connectionString = entityBuilder.ToString();
                }
                return _connectionString;
            }
        }

        public BetStorageEntities()
            : base(ConnectionString)
        {
            Configuration.LazyLoadingEnabled = false;
        }
    }
}
