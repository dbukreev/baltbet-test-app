﻿using System.Runtime.Serialization;

namespace Baltbet.TestApp.Entities.BetStorage
{
    [DataContract]
    public class Account
    {
        [DataMember]
        public int AccountId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public decimal Amount { get; set; }
    }
}
