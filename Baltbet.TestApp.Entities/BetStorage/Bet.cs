﻿using System;
using System.Runtime.Serialization;

namespace Baltbet.TestApp.Entities.BetStorage
{
    [DataContract]
    public class Bet
    {
        [DataMember]
        public int BetId { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public byte ResultId { get; set; }

        [DataMember]
        public string ResultName { get; set; }

        [DataMember]
        public decimal WinAmount { get; set; }

        [DataMember]
        public byte TypeId { get; set; }

        [DataMember]
        public string TypeName { get; set; }

        [DataMember]
        public string Formula { get; set; }

        [DataMember]
        public int? EditionNumber { get; set; }
    }
}
