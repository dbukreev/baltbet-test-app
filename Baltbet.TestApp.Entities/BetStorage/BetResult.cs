﻿using System.Runtime.Serialization;

namespace Baltbet.TestApp.Entities.BetStorage
{
    [DataContract]
    public class BetResult
    {
        [DataMember]
        public byte BetResultId { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
