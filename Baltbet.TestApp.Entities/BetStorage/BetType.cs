﻿using System.Runtime.Serialization;

namespace Baltbet.TestApp.Entities.BetStorage
{
    [DataContract]
    public class BetType
    {
        [DataMember]
        public byte BetTypeId { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
